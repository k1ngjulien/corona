import { Chart } from 'chart.js'
import { parse } from 'papaparse'


let infections: Array<number>;
let chart1: Chart;
let chart2: Chart;

async function main() {

  // infections, healed, death, active, tests
	const epidata_raw = await (await fetch("./CovidFaelleDelta.csv")).text();
	const dates_raw = await (await fetch("./Version.csv")).text();

  const epidata = <Array<Array<string>>> parse(epidata_raw).data;
  const sourced_date = <Array<Array<string>>> parse(dates_raw).data;

  epidata.shift(); // remove titles
  const date_labels = epidata.map((data) => data[0].split(" ")[0]);
  infections = epidata.map((data) => parseInt(data[1]));
  const recoveries = epidata.map((data) => parseInt(data[2]));
  const deaths = epidata.map((data) => parseInt(data[3]));

  let active = 0;
  let total_positive = 0;
  let total_cured = 0;
  let total_died = 0;
  for(let i = 0; i < infections.length; i++) {
    active += infections[i]  - recoveries[i];
    total_positive += infections[i];
    total_cured += recoveries[i]
    total_died += deaths[i];
  }

  const runningAvg = calculateAverage(infections, 14);

  document.getElementById('curSick').innerText = String(active);
  document.getElementById('positiv').innerText = String(total_positive);
  document.getElementById('cured').innerText = String(total_cured);
  document.getElementById('dead').innerText = String(total_died);
  document.getElementById('deathRate').innerText = 
    (Math.round(
      100000 *  total_died / total_positive
  ) / 1000) + "%";
  document.getElementById('dataDate').innerText = sourced_date[1][2];

	const ctx  = <HTMLCanvasElement> document.getElementById('chart1')
	chart1 = new Chart(ctx, {
			type: 'line',
			data: {
          labels: date_labels,
					datasets: [{
							label: 'tägliche Infektionen',
              data: infections,
              borderColor: 'rgb(255, 34, 0)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
              label: 14 + ' Tage Mittelwert' ,
              data: runningAvg,
              borderColor: 'rgb(0, 123, 255)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
              label: 'Genesungen/Tag',
              data: recoveries,
              borderColor: 'rgb(0, 204, 85)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
          }]
			},
			options: {
          maintainAspectRatio: false,
					scales: {
							yAxes: [{
                  id: 'A',
                  position: 'left',
									ticks: {
											beginAtZero: true
									}
              }]
					}
			}
	});
  
	const ctx2  = <HTMLCanvasElement> document.getElementById('chart2')
	chart2 = new Chart(ctx2, {
			type: 'line',
			data: {
          labels: date_labels,
					datasets: [{
							label: 'Fälle',
              data: calculateIntegral(infections),
              borderColor: 'rgb(255, 34, 0)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
							label: 'Genesen',
              data: calculateIntegral(recoveries),
              borderColor: 'rgb(0, 123, 255)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}]
			},
			options: {
          maintainAspectRatio: false,
					scales: {
							yAxes: [{
                  id: 'A',
                  position: 'left',
									ticks: {
											beginAtZero: true
									}
              }]
					}
			}
	});
}

document.getElementById("avglen").addEventListener('input', (e) => {
  changeAverage(parseInt((<HTMLInputElement>e.target).value))
})

function changeAverage(days: number) {
  const avg = calculateAverage(infections, days)
  chart1.data.datasets[1].data = avg
  chart1.data.datasets[1].label = days + ' Tage Mittelwert'
  chart1.update()
}

function calculateAverage(data: Array<number>, lenOfAvg: number): Array<number> {
  let ret = []
  for(let i = 0; i < data.length; i++)   {
     let values = data.slice(i - Math.ceil(lenOfAvg / 2), i + Math.floor(lenOfAvg / 2))
     let avg = values.reduce((a,b)=> a + b, 0) / lenOfAvg
     ret.push(avg)
  }

  ret = ret.slice(0, ret.length - Math.floor(lenOfAvg / 2))

  return ret
}

function calculateDerivative(data: Array<number>): Array<number> {
  let ret = []
  
  for(let i = 0; i < data.length -1; i++){
    ret.push(data[i] - data[i-1]) 
  }

  return ret
}

function calculateIntegral(data: Array<number>): Array<number> {
  let ret = [0]
  
  for(let i = 0; i < data.length -1; i++){
    ret.push(data[i] + ret[i]) 
  }

  return ret
}

var checkbox = document.querySelector('input[name=theme]')
checkbox.addEventListener('change', function(){
    if(this.checked) {
        trans()
        document.documentElement.setAttribute('data-theme', 'dark')
    } else {
        trans()
        document.documentElement.setAttribute('data-theme', 'light')
    }
})

let trans = () => {
    document.documentElement.classList.add('transition');
        window.setTimeout(() => {
            document.documentElement.classList.remove('transition')
        }, 500)
    }
main();

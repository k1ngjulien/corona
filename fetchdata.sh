#!/bin/bash

mkdir -p public/

curl https://covid19-dashboard.ages.at/data/data.zip --cipher 'DEFAULT:!DH' --output public/data.zip
unzip public/data.zip -d public/
